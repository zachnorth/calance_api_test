This program will clone the Calance api-test repository to your computer then retrieve all
the commit messages and output them to a test file called "Messages.txt." in the same 
folder as this program. 

To Run:
	1) You need to be able to build and run c# code on your computer
		I used the command "dotnet run" in the directory that contained this program
	2) The program assumes your git.exe file is in your path. If it is not, either include
	   it in your path or direct the program to the correct folder in bot RunProcess and 
	   RunProcessGitClone.
