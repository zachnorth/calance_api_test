﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace CalanceProject
{

    class Program
    {
        static void Main(string[] args)
        {
            RunProcessGitClone($"clone https://bitbucket.org/calanceus/api-test.git");  //Call to run git clone... command

            string thePath = Directory.GetCurrentDirectory(); //Gets current directory to call next git command in

            var result = ListShaWithFiles(thePath); 

            System.IO.File.WriteAllText("Messages.txt", result); //Outputs results to a new .txt file
        }

        //Method to turn the path into a readable string and call
        public static string ListShaWithFiles(string gitDirectory)
        {
            var path = gitDirectory.Replace("\\", "/");

            var output = RunProcess($"--git-dir={path}/api-test/.git --work-tree={path}/api-test log --pretty=format:'%ad%x09%s'");
            return output;
        }

        private static void RunProcessGitClone(string command)
        {
            try
            {
                using (Process p = new Process())
                {
                    //Process that runs "git clone https://bitbucket.org/calanceus/api-test.git

                    p.StartInfo.FileName = "git.exe";       //Assumes git.exe is in your PATH. If not,, replace this with the Path to your git.exe file.
                    p.StartInfo.UseShellExecute = false; //Hides the git clone process information from the command line
                    p.StartInfo.RedirectStandardError = true; //So you dont get "fatal: git repository already exists message in command line"
                    p.StartInfo.Arguments = command;
                    p.Start();

                    p.WaitForExit();
                    
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(".git directory already exists." + e);
            }
            

        }

        private static string RunProcess(string command)
        {
            try
            {
                using (Process p = new Process())
                {

                    //Runs the command to pull all the commit messages to the command line
                    p.StartInfo.FileName = "git.exe";       //Assumes git.exe is in your PATH. If not,, replace this with the Path to your git.exe file.
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true; //Redirects output
                    p.StartInfo.RedirectStandardError = true;
                    p.StartInfo.Arguments = command;
                    p.Start();
                    
                    string output = p.StandardOutput.ReadToEnd();

                    p.WaitForExit();

                    return output;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "Program did not run correctly";
            }
        }
        
    }
}

