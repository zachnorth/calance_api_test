"""
------
Libraries
"""
try:
    import subprocess
except ImportError:
    raise ImportError('Must have subprocess installed. Try "pip install subprocess"')
try:
    import tkinter as tk
except ImportError:
    raise ImportError('Must have tkinter install. Try "pip install tkinter"')

"""
------
Define root and setup canvas
"""
root = tk.Tk()

canvas1 = tk.Canvas(root, width = 350, height = 250, bg='light cyan', bd=2, relief='groove')
canvas1.pack()


""""
------
Methods
"""

"""Method to run .bat script"""
def start_batch():
    subprocess.run([r"script.bat"])
    label= tk.Label(root, text="Messages saved to: Messages.txt")
    label.pack()
    root.after(2000, widget_destroyer, label)

"""Method to close destination message"""
def widget_destroyer(widget):
    widget.destroy()



"""
------
Buttons
"""

"""Button to run .bat script, find the commits, and output them to a file"""
button1 = tk.Button (root, text='Find the messages', command=start_batch, bg='pale green')
canvas1.create_window(140, 130, window=button1)


"""Quit button"""
button2 = tk.Button(root, text='Quit', command = root.quit, bg='tomato')
canvas1.create_window(210, 130, window=button2)

root.mainloop()