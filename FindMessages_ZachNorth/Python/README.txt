This program will query the Calance bitbucket repository and output the commit messages to a text file in the same folder as the program.

To run:
    1) You need the subprocess python package installed ('pip install subprocess')
    2) You need the tkinter python package install ('pip install tkinter')
    3) You must be running on a Windos OS in order to run the .bat scripts


Created by: Zachary North
Date: 02/15/2020
